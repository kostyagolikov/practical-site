<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require "database.php";
require "search.php";

$parameters = [];
if(!empty($_GET['category'])) {
    $parameters['category'] = $_GET['category'];
}
if(!empty($_GET['price_range'])) {
    $priceRange = explode('_', $_GET['price_range']);
    if(array_key_exists(0, $priceRange)) {
        $parameters['price_gte'] = $priceRange[0]; 
    }
    if (array_key_exists(1, $priceRange) && (int)$priceRange[1] > 0) {
        $parameters['price_lte'] = $priceRange[1];
    } 
}
if(isset($_GET['reset'])) {
  header('Location: http://localhost:8000'); 
  exit();
}

$products = selectProducts($parameters);

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Shoe Market</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- Animation CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    <!--Our custom styles-->
    <link rel="stylesheet" href="dist/css/custom.css">
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

            <!-- SEARCH FORM -->
            <form class="form-inline ml-3">
                <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                    <div class="input-group-append">
                        <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
                    </div>
                </div>
            </form>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="index.php" class="brand-link">
                <img src="dist/img/AdminLTELogo.png" alt="Deposit Management Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">Deposit Management</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="dist/img/user9-.jpg" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block">Admin Constantin</a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                        <li class="nav-item has-treeview menu-open">
                        <a href="#" class="nav-link">
                        <i class="nav-icon fa fa-archive" aria-hidden="true"></i>
                        <p>
                            Products list
                            <i class="right fas fa-angle-left"></i>
                        </p>
                        </a>
                        <ul class="nav nav-treeview" style="display: block;">
                        <li class="nav-item">
                            <a href="index.php" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Nike</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="index.php" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Puma</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="index.php" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Adidas</p>
                            </a>
                        </li>
                    </ul>
                        </li>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Products List</h1>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Products List</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                        <?php if(array_key_exists('error', $_GET)):?>
                            <div class="alert alert-danger animate__animated" id="alert-danger" role="alert">
                                Wrong create or edit your product! Please try again!
                            </div>
                        <?php elseif(array_key_exists('success', $_GET)):?>
                            <div class="alert alert-success animate__animated" id="alert-success" role="alert">
                                You have been saved your product successfully!
                            </div>
                        <?php endif;?>
                        <?php if(array_key_exists('delete', $_GET)):?>
                            <div class="alert alert-danger animate__animated" id="alert-delete" role="alert">
                                You have been deleted your product successfully!
                            </div>
                        <?php endif;?>
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Nike Shoes</h3>
                                    <div class="card-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-inline-flex float-right col-xs-1">
                                        <button 
                                            type="button" 
                                            class="btn btn-block btn-primary table-filters" 
                                            data-toggle="collapse" 
                                            data-target="#filters" 
                                            aria-expanded="false" 
                                            aria-controls="filters">
                                            Filters
                                        </button>    
                                    </div>
                                    <div class="d-inline-flex float-right col-xs-1">
                                        <a 
                                            type="button" 
                                            class="btn btn-block btn-success table-filters" 
                                            href="create.php"
                                        >
                                            Create
                                        </a>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="collapse" id="filters">
                                    <div class="card-body">
                                        <form class="row">
                                            <div class="form-group col-3">
                                                <label>Category</label>
                                                <select class="form-control" name="category">
                                                    <option value=''>All</option>
                                                    <option value="Sneakers" <?php if(!empty($_GET['category']) && $_GET['category'] === 'Sneakers') echo 'selected' ?>>Sneakers</option>
                                                    <option value="Running Shoes" <?php if(!empty($_GET['category']) && $_GET['category'] === 'Running Shoes') echo 'selected' ?>>Running Shoes</option>
                                                    <option value="Basketball Shoes" <?php if(!empty($_GET['category']) && $_GET['category'] === 'Basketball Shoes') echo 'selected' ?>>Basketball Shoes</option>
                                                    <option value="Walking Shoes" <?php if(!empty($_GET['category']) && $_GET['category'] === 'Walking Shoes') echo 'selected' ?>>Walking Shoes</option>
                                                    <option value="Boots" <?php if(!empty($_GET['category']) && $_GET['category'] === 'Boots') echo 'selected' ?>>Boots</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-3">
                                                <label>Price</label>
                                                <select class="form-control" name='price_range'>
                                                    <option value=''>All</option>
                                                    <option value="10_20" <?php if(!empty($_GET['price_range']) && $_GET['price_range'] === '10_20') echo 'selected' ?>>10-20$</option>
                                                    <option value="21_30" <?php if(!empty($_GET['price_range']) && $_GET['price_range'] === '21_30') echo 'selected' ?>>21-30$</option>
                                                    <option value="31_40" <?php if(!empty($_GET['price_range']) && $_GET['price_range'] === '31_40') echo 'selected' ?>>31-40$</option>
                                                    <option value="41_50" <?php if(!empty($_GET['price_range']) && $_GET['price_range'] === '41_50') echo 'selected' ?>>41-50$</option>
                                                    <option value="50_" <?php if(!empty($_GET['price_range']) && $_GET['price_range'] === '50_') echo 'selected' ?>>50$+</option>
                                                </select>
                                            </div>
                                            <div class="form-group align-self-end col-1">
                                                <button type="submit" class="btn btn-block btn-primary">Filter</button>
                                            </div>
                                            <div class="form-group align-self-end col-1">
                                                <button type="submit" class="btn btn-block bg-gradient-danger" name="reset">Reset</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-head-fixed text-nowrap table-striped product-list-table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>ID</th>
                                                <th>Image</th>
                                                <th>Name</th>
                                                <th>Category</th>
                                                <th>Price</th>
                                                <th>Sale Price</th>
                                                <th>Available Quantity</th>
                                                <th>Description</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($products as $product):?>
                                            <tr>
                                                <td>
                                                    <a class="mr-2" href="edit.php?id=<?= $product['id']?>"><i class="fas fa-pencil-alt"></i></a>
                                                    <a href="delete.php?id=<?= $product['id']?>&_method=DELETE"><i class="fas fa-trash-alt"></i></a>
                                                </td>
                                                <td><?php echo $product['id']?></td>
                                                <td><img src="<?= $product['image_path']?>"/></td>
                                                <td><?= $product['name']?></td>
                                                <td><?= $product['category']?></td>
                                                <td><?= $product['price']?>$</td>
                                                <td><?= $product['price'] - $product['discount']?>$</td>
                                                <td><?= $product['quantity']?> pairs</td>
                                                <td><?= $product['description']?></td>
                                            </tr> 
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer clearfix">
                                    <ul class="pagination pagination-sm m-0 float-right">
                                        <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                                    </ul>
                                </div> 
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="float-right d-none d-sm-inline">
                PRO IT ACADEMY
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>
    <!-- Alert Edit/Create-->                                           
    <script>
        let alertSuccess = document.getElementById('alert-success')
        let alertDanger = document.getElementById('alert-danger')

        setTimeout(function() {
            if(alertSuccess !== null) {
                alertSuccess.classList.add('animate__fadeOut')
                $('#alert-success').hide(500);   
            }
            if(alertDanger !== null) {
                alertDanger.classList.add('animate__fadeOut')
                $('#alert-danger').hide(500);   
            }
        }, 1300);
        window.history.replaceState({}, document.title, "http://localhost:8000")
    </script>
    <!-- Alert Delete -->
    <script>
        let alertDelete = document.getElementById('alert-delete')

        setTimeout(function() {
            if(alertDelete !== null) {
                alertDelete.classList.add('animate__fadeOut')
                $('#alert-delete').hide(500);   
            }
        }, 1300);
        window.history.replaceState({}, document.title, "http://localhost:8000")
    </script>
</body>

</html>