<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require "database.php";

try {
    if (array_key_exists('id', $_GET) && array_key_exists('_method', $_GET) && $_GET['_method'] === 'DELETE') {
        $product = selectProduct((int)$_GET['id']);
        if (file_exists($product['image_path'])) {
            unlink($product['image_path']);
        }
        deleteProduct($_GET['id']);
    }
}
catch(\Exception $e) {
    header('Location: http://localhost:8000?delete=true');
    return;
}

header('Location: http://localhost:8000?delete=true');