<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);


require "database.php";

try {
    if(array_key_exists('_method', $_POST) && $_POST['_method'] === 'POST') {
        saveImage();
        createProduct($_POST);
    } elseif (array_key_exists('id', $_GET) && array_key_exists('_method', $_POST) && $_POST['_method'] === 'PATCH') {
        saveImage();
        editProduct($_GET['id'], $_POST);
    }    
}
catch(\Exception $e) {
    header('Location: http://localhost:8000?error=true');
    return;
}

function saveImage(){
    $_POST['product_image'] = array_key_exists('product_image_old', $_POST) ? $_POST['product_image_old'] : '';
    if (array_key_exists('product_image', $_FILES) && $_FILES['product_image']['size'] > 0) {
        $uploadsDir = 'dist/img/products/';
        $fileName = $uploadsDir . basename($_FILES['product_image']['name']);
        if (!file_exists($fileName)) {
            move_uploaded_file($_FILES['product_image']['tmp_name'], $fileName);
        }
        if (array_key_exists('product_image_old', $_POST) && $_POST['product_image_old'] !== $_POST['product_image']) {
            unlink($_POST['product_image_old']);
        }
        $_POST['product_image'] = $fileName;
    }

    return $fileName;
}


header('Location: http://localhost:8000?success=true');